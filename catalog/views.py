from django.shortcuts import render
from django.http import HttpResponse
from .models import RouterIP, MickroTikCommand, OltIP, OLTCommand
import paramiko
import routeros_api
import json
import telnetlib
import sys, re

def terminal_comands(request):
    
    data = MickroTikCommand.objects.values_list('comandname', flat=True)
    routerdata = RouterIP.objects.values_list('name', flat=True)
    olt_terminal_commands = OLTCommand.objects.values_list('oltcommand', flat=True)
    oltip = OltIP.objects.values_list('oltname', flat=True)
    print(oltip)
    
    print(routerdata)
    if request.method == "GET" or "POST":


        return render(request, 'newindex.html',  {"routerdata": routerdata, "itemdata": data, 'oltcommand': olt_terminal_commands, 'oltip':oltip})

    else:
        print(request.POST)
        url_routername = request.POST['routername']
        url_command = request.POST['commands']
        url_pot = request.POST['port']
            
        print(url_command)

        source = RouterIP.objects.get(name=url_routername)
        target = source.ipaddress
        temp = url_command.replace('_', ' ').upper()
        print(temp)
        execute_command = MickroTikCommand.objects.get(comandname=temp)
        print(execute_command)
        command = execute_command.command
        print(command)

        print(target)

        clinet = paramiko.SSHClient()
        clinet.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        clinet.connect(target, username='admin', password='admin1', look_for_keys=False)

        stdin,stdout,stderr = clinet.exec_command(command)
        result = stdout.read().decode()
        # result = re.sub(r'\s+', " ", result)
        clinet.close()
        # context = {
        #     "form": result
        # }      

    return render(request, 'newindex.html', {"form": result, "routerdata": routerdata, "itemdata": data})





    

def terminal_list(request):
    
    if  len(request.GET) == 0:

        olt_terminal_commands = OLTCommand.objects.values_list('oltcommand', flat=True)
        oltip = OltIP.objects.values_list('oltname', flat=True)
        html_dict = {
            'oltcommand': olt_terminal_commands,
            'oltip':oltip 
        }
        return render(request, 'index3.html', html_dict )
    
    else:
        olt_terminal_commands = OLTCommand.objects.values_list('oltcommand', flat=True)
    
        oltip = OltIP.objects.values_list('oltname', flat=True)
        print(olt_terminal_commands)
          
        olt_ip = request.GET['olt_name']
        olt_pon = request.GET['olt_pon']
        olt_onu = request.GET['olt_onu']
        olt_command = request.GET['olt_terminal_commands']
        olt_input_command = OLTCommand.objects.get(oltcommand=olt_command)
        olt_output_command = olt_input_command.olt_commands
        print(olt_output_command)
        olt_data = OltIP.objects.get(oltname=olt_ip)
        olt_source = olt_data.oltaddress

        # print(olt_ip, ">>>", olt_pon + " >>>" , olt_onu)
        HOST = olt_source
        PON = olt_pon
        tn=telnetlib.Telnet(HOST)
        tn.write(b'script\n')
        tn.write(b'7#Ui1WUmqu\n')
        tn.write(b'enable\n')
        tn.write(b'7#Ui1WUmqu\n')
        tn.write(b"configure terminal\n")
        tn.write(b"interface epon 0/" + PON.encode('ascii') + b"\n")
        print("START COMMAND")
        tn.write(olt_output_command.encode('ascii') + b"\n") 
        print("END COMMAND")
        tn.write(b"end \n")
        tn.write(b"exit \n")
        tn.write(b"exit \n")
        x = tn.read_all().decode('ascii')
        print (x)


        content = { 
            "result": x , 
            'oltcommand': olt_terminal_commands,
            'oltip':oltip 
        }

        
        return render(request, 'index3.html', content )
        