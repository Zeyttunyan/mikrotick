from django.contrib import admin
from .models import RouterIP, OltIP, MickroTikCommand,OLTCommand

class Router(admin.ModelAdmin):
    tabel = ('name', 'ipaddress')

class Oltname(admin.ModelAdmin):
    dysplay = ('oltname', 'oltaddress')


class MikroTik(admin.ModelAdmin):
    screen = ('comandname', 'commands')

class OLT(admin.ModelAdmin):
    class Meta:

        oltdisplay = ('oltcommand', 'olt_commands')

admin.site.register(RouterIP)
admin.site.register(OltIP)
admin.site.register(MickroTikCommand)
admin.site.register(OLTCommand)



