from django.urls import path
from . import views

# app_name = 'routers'

urlpatterns = [
    path('', views.terminal_comands),
    path('olt', views.terminal_list),
    # path('', views.terminal_process),
    path('<int:olt>/', views.terminal_list),
    # path('', views.commands),

]