from django.db import models



class RouterIP(models.Model):
    name = models.CharField(max_length = 200)
    ipaddress = models.GenericIPAddressField(default='0.0.0.0')

    def __str__(self):
        return self.name


    def show_desc(self):
        return self.ipaddress

class OltIP(models.Model):
    oltname = models.CharField(max_length = 200)
    oltaddress = models.GenericIPAddressField(default='0.0.0.0')

    def __str__(self):
        return self.oltname

    def show_desc(self):
        return self.oltaddress

class MickroTikCommand(models.Model):
    comandname = models.CharField(max_length=400)
    command = models.CharField(max_length=400)

    def __str__(self):
        return self.comandname

    def show_desc(self):
        return self.command

class OLTCommand(models.Model):
    oltcommand = models.CharField(max_length=400)
    olt_commands = models.CharField(max_length=400)

    def __str__(self):
        return self.oltcommand

    def show_desc(self):
        return self.olt_commands